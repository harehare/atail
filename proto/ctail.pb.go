// Code generated by protoc-gen-go. DO NOT EDIT.
// source: proto/ctail.proto

package ctail

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	context "golang.org/x/net/context"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	grpc "google.golang.org/grpc"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type TailRequest struct {
	FileName             string   `protobuf:"bytes,1,opt,name=file_name,json=fileName,proto3" json:"file_name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TailRequest) Reset()         { *m = TailRequest{} }
func (m *TailRequest) String() string { return proto.CompactTextString(m) }
func (*TailRequest) ProtoMessage()    {}
func (*TailRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_ec1e4b3fcc096d50, []int{0}
}

func (m *TailRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TailRequest.Unmarshal(m, b)
}
func (m *TailRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TailRequest.Marshal(b, m, deterministic)
}
func (m *TailRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TailRequest.Merge(m, src)
}
func (m *TailRequest) XXX_Size() int {
	return xxx_messageInfo_TailRequest.Size(m)
}
func (m *TailRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_TailRequest.DiscardUnknown(m)
}

var xxx_messageInfo_TailRequest proto.InternalMessageInfo

func (m *TailRequest) GetFileName() string {
	if m != nil {
		return m.FileName
	}
	return ""
}

type TailResponse struct {
	Data                 string   `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TailResponse) Reset()         { *m = TailResponse{} }
func (m *TailResponse) String() string { return proto.CompactTextString(m) }
func (*TailResponse) ProtoMessage()    {}
func (*TailResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_ec1e4b3fcc096d50, []int{1}
}

func (m *TailResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TailResponse.Unmarshal(m, b)
}
func (m *TailResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TailResponse.Marshal(b, m, deterministic)
}
func (m *TailResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TailResponse.Merge(m, src)
}
func (m *TailResponse) XXX_Size() int {
	return xxx_messageInfo_TailResponse.Size(m)
}
func (m *TailResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_TailResponse.DiscardUnknown(m)
}

var xxx_messageInfo_TailResponse proto.InternalMessageInfo

func (m *TailResponse) GetData() string {
	if m != nil {
		return m.Data
	}
	return ""
}

func init() {
	proto.RegisterType((*TailRequest)(nil), "ctail.TailRequest")
	proto.RegisterType((*TailResponse)(nil), "ctail.TailResponse")
}

func init() { proto.RegisterFile("proto/ctail.proto", fileDescriptor_ec1e4b3fcc096d50) }

var fileDescriptor_ec1e4b3fcc096d50 = []byte{
	// 192 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x2c, 0x28, 0xca, 0x2f,
	0xc9, 0xd7, 0x4f, 0x2e, 0x49, 0xcc, 0xcc, 0xd1, 0x03, 0xb3, 0x85, 0x58, 0xc1, 0x1c, 0x29, 0x99,
	0xf4, 0xfc, 0xfc, 0xf4, 0x9c, 0x54, 0xfd, 0xc4, 0x82, 0x4c, 0xfd, 0xc4, 0xbc, 0xbc, 0xfc, 0x92,
	0xc4, 0x92, 0xcc, 0xfc, 0xbc, 0x62, 0x88, 0x22, 0x25, 0x2d, 0x2e, 0xee, 0x90, 0xc4, 0xcc, 0x9c,
	0xa0, 0xd4, 0xc2, 0xd2, 0xd4, 0xe2, 0x12, 0x21, 0x69, 0x2e, 0xce, 0xb4, 0xcc, 0x9c, 0xd4, 0xf8,
	0xbc, 0xc4, 0xdc, 0x54, 0x09, 0x46, 0x05, 0x46, 0x0d, 0xce, 0x20, 0x0e, 0x90, 0x80, 0x5f, 0x62,
	0x6e, 0xaa, 0x92, 0x12, 0x17, 0x0f, 0x44, 0x6d, 0x71, 0x41, 0x7e, 0x5e, 0x71, 0xaa, 0x90, 0x10,
	0x17, 0x4b, 0x4a, 0x62, 0x49, 0x22, 0x54, 0x1d, 0x98, 0x6d, 0x14, 0xc0, 0xc5, 0xe3, 0x92, 0x58,
	0x92, 0x18, 0x52, 0x94, 0x98, 0x57, 0x9c, 0x96, 0x5a, 0x24, 0xe4, 0xc0, 0xc5, 0x02, 0x72, 0x85,
	0x90, 0x90, 0x1e, 0xc4, 0x69, 0x48, 0x96, 0x49, 0x09, 0xa3, 0x88, 0x41, 0x0c, 0x55, 0xe2, 0x6d,
	0xba, 0xfc, 0x64, 0x32, 0x13, 0xbb, 0x10, 0xab, 0x3e, 0x48, 0xce, 0x80, 0x31, 0x89, 0x0d, 0xec,
	0x50, 0x63, 0x40, 0x00, 0x00, 0x00, 0xff, 0xff, 0x62, 0x63, 0x26, 0xcb, 0xe2, 0x00, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// DataTransferClient is the client API for DataTransfer service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type DataTransferClient interface {
	Tail(ctx context.Context, in *TailRequest, opts ...grpc.CallOption) (DataTransfer_TailClient, error)
}

type dataTransferClient struct {
	cc *grpc.ClientConn
}

func NewDataTransferClient(cc *grpc.ClientConn) DataTransferClient {
	return &dataTransferClient{cc}
}

func (c *dataTransferClient) Tail(ctx context.Context, in *TailRequest, opts ...grpc.CallOption) (DataTransfer_TailClient, error) {
	stream, err := c.cc.NewStream(ctx, &_DataTransfer_serviceDesc.Streams[0], "/ctail.DataTransfer/tail", opts...)
	if err != nil {
		return nil, err
	}
	x := &dataTransferTailClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type DataTransfer_TailClient interface {
	Recv() (*TailResponse, error)
	grpc.ClientStream
}

type dataTransferTailClient struct {
	grpc.ClientStream
}

func (x *dataTransferTailClient) Recv() (*TailResponse, error) {
	m := new(TailResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// DataTransferServer is the server API for DataTransfer service.
type DataTransferServer interface {
	Tail(*TailRequest, DataTransfer_TailServer) error
}

func RegisterDataTransferServer(s *grpc.Server, srv DataTransferServer) {
	s.RegisterService(&_DataTransfer_serviceDesc, srv)
}

func _DataTransfer_Tail_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(TailRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(DataTransferServer).Tail(m, &dataTransferTailServer{stream})
}

type DataTransfer_TailServer interface {
	Send(*TailResponse) error
	grpc.ServerStream
}

type dataTransferTailServer struct {
	grpc.ServerStream
}

func (x *dataTransferTailServer) Send(m *TailResponse) error {
	return x.ServerStream.SendMsg(m)
}

var _DataTransfer_serviceDesc = grpc.ServiceDesc{
	ServiceName: "ctail.DataTransfer",
	HandlerType: (*DataTransferServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "tail",
			Handler:       _DataTransfer_Tail_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "proto/ctail.proto",
}
