package main

import (
	"context"
	"log"
	"os"
	"strings"
	"sync"

	proto "github.com/harehare/ctail/proto"
	"github.com/urfave/cli"
	"google.golang.org/grpc"
)

func main() {
	app := cli.NewApp()
	app.Name = "ctail"
	app.Usage = "Collect remote log in your terminal"
	app.Version = "0.0.1"
	app.Commands = []cli.Command{
		{
			Name:    "start",
			Aliases: []string{"r"},
			Usage:   "",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "s",
					Value: ":0",
					Usage: "server name",
				},
				cli.StringFlag{
					Name:  "a",
					Value: "",
					Usage: "allow file patterns",
				},
				cli.StringFlag{
					Name:  "c",
					Value: "",
					Usage: "tls crt file",
				},
				cli.StringFlag{
					Name:  "k",
					Value: "",
					Usage: "tls key file",
				},
			},
			Action: func(c *cli.Context) error {
				return Serve(c.String("s"), c.String("c"), c.String("k"), strings.Split(c.String("a"), ","))
			},
		},
		{
			Name:    "tail",
			Aliases: []string{"t"},
			Usage:   "",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "s",
					Value: "",
					Usage: "server names",
				},
				cli.StringFlag{
					Name:  "f",
					Value: "",
					Usage: "file name. if file name is empty then stdin",
				},
			},
			Action: func(c *cli.Context) error {
				if strings.Trim(c.String("s"), " ") == "" {
					log.Fatalln("server names is required.", "-s 127.0.0.1:3000,192.168.1.2:3000")
				}

				ctx := context.Background()
				wg := &sync.WaitGroup{}
				fileName := c.String("f")
				config := LoadConfig()
				StartWorker(config)

				for _, serverName := range strings.Split(c.String("s"), ",") {
					wg.Add(1)
					go func() {
						conn, err := grpc.Dial(serverName, grpc.WithInsecure())
						if err != nil {
							log.Fatalf("%v", err)
							wg.Done()
							return
						}
						defer conn.Close()

						d := NewTail(ctx, proto.NewDataTransferClient(conn), serverName, fileName)
						d.start(wg)
					}()
				}
				wg.Wait()
				return nil
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
