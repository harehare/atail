package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type Config struct {
	ErrorRegex string `json:"error"`
	WarnRegex  string `json:"warn"`
}

func LoadConfig() (config Config) {
	bytes, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Fatal(err)
	}
	if err := json.Unmarshal(bytes, &config); err != nil {
		log.Fatal(err)
	}

	return
}
