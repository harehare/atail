package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"regexp"
	"strings"
	"sync"

	proto "github.com/harehare/ctail/proto"
	"gopkg.in/fatih/color.v1"
)

type tail struct {
	client     proto.DataTransferClient
	ctx        context.Context
	serverName string
	fileName   string
}

type PrintData struct {
	text       string
	serverName string
}

var PrintQueue = make(chan PrintData, 1000)

func StartWorker(config Config) {
	infoLog := color.New(color.FgWhite).Add(color.BgBlue)
	warnLog := color.New(color.FgBlack).Add(color.BgYellow)
	errLog := color.New(color.FgBlack).Add(color.BgRed)

	errorRegex := regexp.MustCompile(config.ErrorRegex)
	warnRegex := regexp.MustCompile(config.WarnRegex)

	go func() {
		for {
			data := <-PrintQueue
			row := strings.ToLower(data.text)

			if errorRegex.MatchString(row) {
				errLog.Print(" " + data.serverName + " ")
			} else if warnRegex.MatchString(row) {
				warnLog.Print(" " + data.serverName + " ")
			} else {
				infoLog.Print(" " + data.serverName + " ")
			}
			fmt.Println(" " + data.text)
		}
	}()
}

func NewTail(ctx context.Context, client proto.DataTransferClient, serverName, fileName string) *tail {
	t := &tail{
		ctx:        ctx,
		client:     client,
		serverName: serverName,
		fileName:   fileName,
	}
	return t
}

func (t *tail) start(wg *sync.WaitGroup) {
	req := &proto.TailRequest{
		FileName: t.fileName,
	}

	data, err := t.client.Tail(t.ctx, req)

	if err != nil {
		log.Printf("%v", err)
		wg.Done()
		return
	}

	for {
		res, err := data.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Printf("%v", err)
			break
		}
		if err != nil {
			log.Printf("%v", err)
			break
		}
		PrintQueue <- PrintData{text: res.Data, serverName: t.serverName}
	}

	wg.Done()
	data.CloseSend()
}
