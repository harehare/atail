package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"regexp"

	"github.com/fatih/color"
	"github.com/gorilla/handlers"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	proto "github.com/harehare/ctail/proto"
	tailtool "github.com/hpcloud/tail"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type DataTransferService struct {
	allowFilePatterns []string
}

func (d *DataTransferService) Tail(req *proto.TailRequest, stream proto.DataTransfer_TailServer) error {

	if req.FileName == "" {

		stdin := bufio.NewScanner(os.Stdin)
		for stdin.Scan() {
			text := stdin.Text()
			res := &proto.TailResponse{Data: text}
			if err := stream.Send(res); err != nil {
				return err
			}
		}

	} else {

		if len(d.allowFilePatterns) > 0 {
			isAllowFile := false
			for _, re := range d.allowFilePatterns {
				if regexp.MustCompile(re).MatchString(req.FileName) {
					isAllowFile = true
					break
				}
			}

			if !isAllowFile {
				return errors.New(req.FileName + " is not allowed access")
			}
		}

		t, err := tailtool.TailFile(req.FileName, tailtool.Config{Follow: true})

		if err != nil {
			return err
		}

		for line := range t.Lines {
			res := &proto.TailResponse{Data: line.Text}
			if err := stream.Send(res); err != nil {
				return err
			}
		}
	}

	return nil
}

func restServe(endpoint string) error {

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	newMux := handlers.CORS(
		handlers.AllowedMethods([]string{"GET"}),
		handlers.AllowedOrigins([]string{"*"}),
	)(mux)
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := proto.RegisterDataTransferHandlerFromEndpoint(ctx, mux, endpoint, opts)
	if err != nil {
		return err
	}

	notice := color.New(color.FgBlack)
	notice = notice.Add(color.BgHiYellow)
	notice.Print(" HTTP ")
	fmt.Println("  :8080")
	return http.ListenAndServe(":8080", newMux)
}

func Serve(host, tlsCertFile, tlsKeyFile string, allowFilePatterns []string) error {
	listener, err := net.Listen("tcp", host)
	defer listener.Close()

	if err != nil {
		return err
	}

	options := []grpc.ServerOption{}

	if tlsCertFile != "" && tlsKeyFile != "" {
		creds, err := credentials.NewServerTLSFromFile(
			tlsCertFile,
			tlsKeyFile)
		if err != nil {
			return err
		}
		options = append(options, grpc.Creds(creds))
	}

	log.Println("ctail started")

	notice := color.New(color.FgWhite)
	notice = notice.Add(color.BgGreen)
	notice.Print(" OPEN ")
	fmt.Println("  ", listener.Addr().String())

	go func() {
		restServe(listener.Addr().String())
	}()

	server := grpc.NewServer(options...)
	proto.RegisterDataTransferServer(server, &DataTransferService{allowFilePatterns: allowFilePatterns})
	return server.Serve(listener)
}
